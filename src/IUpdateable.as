// author : Leon Ho
package
{
	public interface IUpdateable
	{
		function update(deltaTime:Number):void;
	}
}