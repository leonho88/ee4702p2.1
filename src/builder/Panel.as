// author : Leon Ho
package builder
{	
	import ai.Blackboard;
	
	import com.sibirjak.asdpc.listview.*;
	import com.sibirjak.asdpc.scrollbar.*;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	/*
	Panel
	A panel similar to object explorers in many other applications that allows inspection and modification of an object's attributes
	related classes ICanvasObject.as
	*/
	public class Panel extends Sprite implements IUpdateable, IDrawable
	{
		private var panelTextFormat:TextFormat;
		private var headerTextFormat:TextFormat;

		private var worldAttributePanel:Sprite;
		private var selectionAttributePanel:Sprite;
		private var canvasAttributePanel:Sprite;
		private var dialogPanel:Sprite;
		
		private var canvas:Canvas;
		
		private var currentCanvasObject:ICanvasObject = null;
		
		private var binding:Dictionary = new Dictionary();
		
		private var padding:uint = 3;
		
		private var showField:Boolean = true;
		
		public function Panel(canvas:Canvas)
		{
			super();
			
			this.canvas = canvas;
			
			panelTextFormat = new TextFormat("Helvetica", 12);
			headerTextFormat = new TextFormat("Helvetica", 12, null, true);
			
			/*worldAttributePanel = new Sprite();
			worldAttributePanel.y = 10;
			addChild(worldAttributePanel);
			worldAttributePanel.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addFieldsForPanel(worldAttributePanel, canvas.worldAttributes);
			
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFFFFFF, 1.0);
			this.graphics.drawRect(20, 0, 220, 350);
			this.graphics.endFill();
			this.graphics.lineStyle();*/
			
			selectionAttributePanel = new Sprite();
			selectionAttributePanel.y = 10;
			addChild(selectionAttributePanel);
			selectionAttributePanel.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addFieldsForPanel(selectionAttributePanel, null);
			
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFFFFFF, 1.0);
			this.graphics.drawRect(20, 0, 220, 350);
			this.graphics.endFill();
			this.graphics.lineStyle();
			
			canvasAttributePanel = new Sprite();
			addChild(canvasAttributePanel);
			canvasAttributePanel.y = 382;
			canvasAttributePanel.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addFieldsForPanel(canvasAttributePanel, Blackboard.sharedBB().properties);
			
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFFFFFF, 1.0);
			this.graphics.drawRect(20, 372, 220, 140);
			this.graphics.endFill();
			this.graphics.lineStyle();
			
			dialogPanel = new Sprite();
			addChild(dialogPanel);
			dialogPanel.y = 450;
			dialogPanel.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addFieldsForPanel(dialogPanel, null);
			
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFFFFFF, 1.0);
			this.graphics.drawRect(-512, 540, 752, 140);
			this.graphics.endFill();
			this.graphics.lineStyle();
		}
		
		public function showDialog(str:String):void
		{
			var currDialog:TextField = dialogPanel.getChildByName("dialog") as TextField;
			currDialog.text = str;
		}
		 
		private function addFieldsForPanel(panel:DisplayObjectContainer, obj:Object):void
		{
			var header:TextField = new TextField();
			header.defaultTextFormat = headerTextFormat;
			header.selectable = false;
			header.autoSize = TextFieldAutoSize.NONE;
			header.x = 30;
			panel.addChild(header);
			
			var dialog:TextField = new TextField();
			dialog.name = "dialog";
			dialog.defaultTextFormat = panelTextFormat;
			dialog.selectable = false;
			dialog.autoSize = TextFieldAutoSize.LEFT;
			dialog.x = -495;
			dialog.y = 130;
			
			switch (panel) {
				case selectionAttributePanel:
					header.text = "Inspector";
					break;
				case canvasAttributePanel:
					header.text = "Blackboard";
					break;
				case dialogPanel:
					header.text = "Dialog";
					header.x = -495;
					header.y = 100;
					panel.addChild(dialog);
					break;
			}
			
			for (var attr:String in obj)
			{
				var label:TextField = new TextField();
				label.text = attr;
				label.defaultTextFormat = panelTextFormat;
				label.selectable = false;
				label.autoSize = TextFieldAutoSize.RIGHT;
				panel.addChild(label);
				
				label.y = panel.numChildren*(label.textHeight + padding);
				
				if (attr == "items") {					
					var listView:ListView = new ListView();
					listView.setSize(100, 40);
					listView.x = label.x + label.width + padding;
					listView.y = label.y;
					listView.dataSource = obj[attr];
					panel.addChild(listView);
					
					padding = 4;
					showField = false;
				}
				else {
					padding = 3;
					showField = true;
				}
				
				var field:TextField = new TextField();
				field.type = TextFieldType.INPUT;
				field.border = true;
				field.defaultTextFormat = panelTextFormat;
				field.text = "" + obj[attr];
				field.autoSize = TextFieldAutoSize.NONE;
				field.height = field.textHeight + padding;
				field.visible = showField;
				panel.addChild(field);
								
				field.x = label.x + label.width + padding;
				field.y = label.y;
				
				binding[field] = attr;
			}
		}
		
		private function removeFields():void
		{
			selectionAttributePanel.removeChildren();
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.ENTER)
			{
				var field:TextField = e.target as TextField;
				var boundAttribute:String = binding[field];
				var value:Object;
				
				if (boundAttribute == "name" || boundAttribute == "type" || boundAttribute == "questCompleted") {
					value = field.text as String;
				}
				else {
					value = Number(field.text);
				}
				
				if ((value is Number && isNaN(Number(value))) || (value is String && value == ""))
				{
					Game.console.log(boundAttribute + " cannot be set.");
					return;
				}
				
				if (value is String && boundAttribute == "type" && value == "hostile" && currentCanvasObject.attributes.questGiver == 1) {
					Game.console.log(boundAttribute + " cannot be hostile for quest giver.");
					return;
				}
				
				if (e.currentTarget == selectionAttributePanel)
				{
					currentCanvasObject.attributes[boundAttribute] = value;
				}
				else
				{
					//canvas.attributes[boundAttribute] = value;
					Blackboard.sharedBB().setProperty(boundAttribute, value);
				}
			}
		}
		
		public function update(deltaTime:Number):void
		{
			if (currentCanvasObject != canvas.selectedObject)
			{
				removeFields();
				currentCanvasObject = canvas.selectedObject;
				if (currentCanvasObject)
					addFieldsForPanel(selectionAttributePanel, currentCanvasObject.attributes);
			}
			
			// get dialog from manager
			showDialog(canvas.manager.updateDialog(deltaTime));
		}
		
		public function draw(deltaTime:Number):void
		{
			
		}
	}
}