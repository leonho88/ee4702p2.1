package builder
{
	/*
	ICanvasObject
	An interface to allow Canvas to draw information about a selected object, and for Panel to display its attributes
	*/
	public interface ICanvasObject 
	{
		function get attributes():Object;
		
		function get selected():Boolean;
		function set selected(val:Boolean):void;
	}
}