// author : Leon Ho
package builder
{
	import builder.CanvasFile;
	import ai.Manager;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	import flash.utils.ByteArray;
	
	//import utils.CanvasUtils;
	
	public class Canvas extends Sprite implements IUpdateable, IDrawable
	{
		public const scenarioDirectory:String = "scenarios";

		// pixel to meters ratio
		public function get ptm_ratio():Number
		{
			return 1.0/attributes.ptm;
		}
		
		// grid color
		private const color_inactive:uint = 0x696969;
		private const color_hover:uint = 0x000000;
		
		// canvas attributes
		public var attributes:Object = new Object();
		public var worldAttributes:Object = new Object();
		
		// mouse interaction
		private var mousePos:Point = new Point();
		private var trackMouse:Boolean = false;
		
		private var points:Vector.<Point> = new Vector.<Point>();
		private var startPoint:Point = new Point();
		private var endPoint:Point = new Point();
		
		// object vectors
		// walls
		public var objects:Vector.<CanvasObject> = new Vector.<CanvasObject>();
		// sounds
		public var sounds:Vector.<CanvasSound> = new Vector.<CanvasSound>();
		// player avatar
		public var avatar:CanvasAvatar;
		
		// npcs
		public var npcs:Vector.<CanvasNPC> = new Vector.<CanvasNPC>();
		
		// reference for selected context
		public var selectedObject:ICanvasObject;
		
		// simulation and debug
		public static var grid:Boolean = false;
		public static var reverb:Boolean = false;
		public static var occlusion:Boolean = false;
		public static var debug:Boolean = true;
		public static var debugOverlay:DebugOverlay;
		
		// ai manager
		public var manager:Manager;
		
		public function Canvas()
		{
			super();
			
			// size of the canvas
			attributes.size = 512;
			
			// level of points on canvas
			attributes.granularity = 8;
			
			// pixel to meters
			attributes.ptm = 32;
			
			// type of world
			worldAttributes.type = 1;
			
			// avatar
			avatar = new CanvasAvatar(this);
			addChild(avatar);
			avatar.x = attributes.size * 0.5;
			avatar.y = attributes.size * 0.5;
			
			// canvas interactivity
			this.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			this.addEventListener(MouseEvent.CLICK, select);
			
			// debug
			debugOverlay = new DebugOverlay(this);
			addChild(debugOverlay);
			
			this.addEventListener(Event.ADDED, onAdd);
			
			// ai manager
			manager = new Manager(this);
		}
		
		private function onAdd(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			this.removeEventListener(Event.ADDED, onAdd);
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			// move
			if (e.keyCode == Keyboard.W)
			{
				if (e.target == avatar || e.target is CanvasNPC) {
					e.stopImmediatePropagation();
					avatar.moveForward();
				}
			}
			else if (e.keyCode == Keyboard.S)
			{
				if (e.target == avatar || e.target is CanvasNPC) {
					e.stopImmediatePropagation();
					avatar.moveBackward();
				}
			}
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.W || e.keyCode == Keyboard.S)
			{
				avatar.stop();
			}
		}
		
		private function onMouseMove(e:MouseEvent):void
		{
			var half_gran:int = attributes.granularity * 0.5;
			// snap to point
			mousePos.x = (uint)((e.localX + half_gran)/attributes.granularity)*attributes.granularity;
			mousePos.y = (uint)((e.localY + half_gran)/attributes.granularity)*attributes.granularity;
		
			avatar.rotateTo(e.stageX, e.stageY);
			
			/*if (selectedObject is CanvasSound)
			{
				(selectedObject as CanvasSound).rotateTo(e.stageX, e.stageY);
			}*/
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			if (e.target == avatar || e.target is CanvasNPC || e.target is CanvasSound)
			{
				if (e.target is CanvasSound && e.target.attributes.fixed == 1) {
					return;
				}
				e.stopImmediatePropagation();
				(e.target as Sprite).startDrag(false, getRect(this));
			}
			else
			{
				trackMouse = true;
				
				startPoint.x = mousePos.x;
				startPoint.y = mousePos.y;
			}

		}
		
		private function onMouseUp(e:MouseEvent):void
		{	
			if (e.target == avatar || e.target is CanvasNPC ||  e.target is CanvasSound)
			{
				e.stopImmediatePropagation();
				(e.target as Sprite).stopDrag();
			}
			else
			{
				if (!endPoint.equals(startPoint))
					addCanvasObject(startPoint, endPoint, "object" + (objects.length+1));
				
				trackMouse = false;
			}
		}
		
		public function wipe():void
		{
			while (sounds.length > 0)
			{
				removeCanvasSound(sounds[0]);
			}
			while (objects.length > 0)
			{
				removeCanvasObject(objects[0]);
			}
		}
		
		public function addCanvasSound(position:Point, width:int, height:int, sndType:String):void
		{
			var sound:CanvasSound = new CanvasSound(this, position);
			sound.attributes.width = width;
			sound.attributes.height = height;
			sound.attributes.type = sndType;
			sounds.push(sound);
			addChild(sound);
		}
		
		public function removeCanvasSound(snd:CanvasSound):void
		{
			sounds.splice(sounds.indexOf(snd), 1);
			removeChild(snd);
			snd.removeEventListener(MouseEvent.CLICK, select);
		}
		
		public function addCanvasNPC(position:Point, quest:int):void
		{
			var npc:CanvasNPC = new CanvasNPC(this);
			npc.x = position.x;
			npc.y = position.y;
			npc.attributes.questGiver = quest;
			npcs.push(npc);
			npc.brain.id = String(quest);
			addChild(npc);
		}
		
		public function addCanvasObject(start:Point, end:Point, name:String):void
		{
			var newObject:CanvasObject = new CanvasObject(this, start.clone(), end.clone(), name);
			objects.push(newObject);
			addChild(newObject);
		}
		
		public function removeCanvasObject(obj:CanvasObject):void
		{
			objects.splice(objects.indexOf(obj), 1);
			removeChild(obj);
			obj.removeEventListener(MouseEvent.CLICK, select);
		}
		
		private function select(e:MouseEvent):void
		{
			if (e.target is ICanvasObject)
			{
				if (selectedObject) selectedObject.selected = false;
				selectedObject = e.target as ICanvasObject;
				selectedObject.selected = true;

				(selectedObject as InteractiveObject).focusRect = false;
				stage.focus = (selectedObject as InteractiveObject);
			}
		}
		
		/*public function updateWorld():void
		{
			// load current world type
			switch (worldAttributes.type) {
				case 1: 
					var directory:File = File.applicationDirectory.resolvePath(scenarioDirectory);
					var list:Array = directory.getDirectoryListing();
					
					for each (var scenario:File in list) {
					if (scenario.name == "scenario1.txt") {
						var xmlLoader:URLLoader = new URLLoader();
						xmlLoader.addEventListener(Event.COMPLETE, loadXML);
						xmlLoader.load(new URLRequest("scenario1.txt"));
					}
				}
					break;
			}
		}*/
		
		private function loadXML(e:Event):void
		{
			CanvasFile.loadFromSaveFile(e.target.data as ByteArray);
		}
		
		public function update(deltaTime:Number):void
		{
			// track the mouse position with snapping
			if (trackMouse)
			{
				endPoint.x = mousePos.x;
				endPoint.y = mousePos.y;
			}
			
			// update all objects in the scene
			for each (var obj:CanvasObject in objects)
			{
				obj.update(deltaTime);
			}
			
			for each (var snd:CanvasSound in sounds)
			{
				snd.update(deltaTime);
			}
			
			avatar.update(deltaTime);
			
			for each (var npc:CanvasNPC in npcs)
			{
				swapChildren(npc, getChildAt(numChildren-2));
				npc.update(deltaTime);
			}
			
			swapChildren(debugOverlay, getChildAt(numChildren-1));
			swapChildren(avatar, getChildAt(numChildren-1));
			
			manager.updateTension(deltaTime);
		}
		
		public function draw(deltaTime:Number):void
		{
			this.graphics.clear();
			
			// draw world bounds
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFFFFFF, 1.0);
			this.graphics.drawRect(0, 0, attributes.size, attributes.size);
			this.graphics.endFill();
			this.graphics.lineStyle();
			
			// draw the grid dots
			if (grid)
			{
				for (var dx:int = 0; dx < attributes.size; dx += attributes.granularity)
				{
					for (var dy:int = 0; dy < attributes.size; dy += attributes.granularity)
					{
						this.graphics.beginFill(color_inactive);
						this.graphics.drawCircle(dx, dy, 1.0);
						this.graphics.endFill();
					}
				}
			}
			
			// draw line for line creation
			if (trackMouse)
			{
				this.graphics.lineStyle(1);
				this.graphics.moveTo(startPoint.x, startPoint.y);
				this.graphics.lineTo(endPoint.x, endPoint.y);
				this.graphics.lineStyle();
			}
			
			// draw all objects in the scene
			for each (var obj:CanvasObject in objects)
			{
				obj.draw(deltaTime);
			}
			
			for each (var snd:CanvasSound in sounds)
			{
				snd.draw(deltaTime);
			}
			
			avatar.draw(deltaTime);
			
			for each (var npc:CanvasNPC in npcs)
			{
				npc.draw(deltaTime);
			}
			
			debugOverlay.draw(deltaTime);
		}
	}
}