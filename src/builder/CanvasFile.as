package builder
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.describeType;
	import flash.xml.XMLNode;
	
	import flashx.textLayout.elements.Configuration;

	public class CanvasFile
	{
		private static var fileRef:FileReference;
		
		private static const NONE:String = "none";
		private static const SAVE:String = "save";
		private static const LOAD:String = "load";
		private static var status:String = NONE;
		
		public function CanvasFile()
		{
		}
		
		public static function save():void
		{	
			var saveFile:XML = generateSaveFile(Game.builder.canvas);
			
			fileRef = new FileReference();
			fileRef.save(saveFile, ".txt");
			status = SAVE;
			configureListeners(fileRef);
		}
		
		private static function generateSaveFile(canvas:Canvas):XML
		{
			var i:int = 0;
			
			do {
				trace(canvas.getChildAt(i));
				i++;
			}
			while (i < canvas.numChildren);
			
			var xml:XML = <canvas />
			
			for each (var obj:CanvasObject in canvas.objects)
			{
				var object:XML = <object/>
					
				var start:XML=<new/>
				start.setName("start");
				
				var xStart:XML=<new/>
				xStart.setName("x");
				xStart.appendChild(obj.start.x);
				start.appendChild(xStart);
				
				var yStart:XML=<new/>
				yStart.setName("y");
				yStart.appendChild(obj.start.y);
				start.appendChild(yStart);
				
				object.appendChild(start);
				
				var end:XML=<new/>
				end.setName("end");

				var xEnd:XML=<new/>
				xEnd.setName("x");
				xEnd.appendChild(obj.end.x);
				end.appendChild(xEnd);
				
				var yEnd:XML=<new/>
				yEnd.setName("y");
				yEnd.appendChild(obj.end.y);
				end.appendChild(yEnd);
				
				object.appendChild(end);
				
				var attributes:XML=<new/>
				attributes.setName("attributes");
				for (var attr:String in obj.attributes)
				{
					var attribute:XML = <new/>
					attribute.setName(attr);
					attribute.appendChild(obj.attributes[attr]);
					attributes.appendChild(attribute);
				}
				object.appendChild(attributes);
				
				xml.appendChild(object);
			}
			
			for each (var npc:CanvasNPC in canvas.npcs)
			{
				var npcObj:XML=<object/>
					
				var npcObjectType:XML=<new/>
				npcObjectType.setName("objectType");
				npcObjectType.appendChild("npc");
				npcObj.appendChild(npcObjectType);
					
				var npcPosition:XML=<new/>
				npcPosition.setName("position");
				
				var xNPCPos:XML=<new/>
				xNPCPos.setName("x");
				xNPCPos.appendChild(npc.x);
				npcPosition.appendChild(xNPCPos);
				
				var yNPCPos:XML=<new/>
				yNPCPos.setName("y");
				yNPCPos.appendChild(npc.y);
				npcPosition.appendChild(yNPCPos);
				
				npcObj.appendChild(npcPosition);
				
				var npcName:XML=<new/>
				npcName.setName("name");
				npcName.appendChild(npc.attributes.name);
				npcObj.appendChild(npcName);
				
				var questGiver:XML=<new/>
				questGiver.setName("questGiver");
				questGiver.appendChild(npc.attributes.questGiver);
				npcObj.appendChild(questGiver);
				
				var mood:XML=<new/>
				mood.setName("mood");
				mood.appendChild(npc.attributes.mood);
				npcObj.appendChild(mood);
				
				var relationship:XML=<new/>
				relationship.setName("relationship");
				relationship.appendChild(npc.attributes.relationship);
				npcObj.appendChild(relationship);
				
				xml.appendChild(npcObj);
			}
			
			for each (var snd:CanvasSound in canvas.sounds)
			{
				var sound:XML=<object/>
					
				var sndObjectType:XML=<new/>
				sndObjectType.setName("objectType");
				sndObjectType.appendChild("sound");
				sound.appendChild(sndObjectType);
				
				var sndPosition:XML=<new/>
				sndPosition.setName("position");
				
				var xSndPos:XML=<new/>
				xSndPos.setName("x");
				xSndPos.appendChild(snd.x);
				sndPosition.appendChild(xSndPos);
				
				var ySndPos:XML=<new/>
				ySndPos.setName("y");
				ySndPos.appendChild(snd.y);
				sndPosition.appendChild(ySndPos);
				
				sound.appendChild(sndPosition);
				
				var sndName:XML=<new/>
				sndName.setName("name");
				sndName.appendChild(snd.attributes.name);
				sound.appendChild(sndName);
				
				var size:XML=<new/>
				size.setName("size");
				
				var width:XML=<new/>
				width.setName("width");
				width.appendChild(snd.attributes.width);
				size.appendChild(width);
				
				var height:XML=<new/>
				height.setName("height");
				height.appendChild(snd.attributes.height);
				size.appendChild(height);
				
				sound.appendChild(size);
				
				var type:XML=<new/>
				type.setName("type");
				type.appendChild(snd.attributes.type);
				sound.appendChild(type);
				
				var fixed:XML=<new/>
				fixed.setName("fixed");
				fixed.appendChild(snd.attributes.fixed);
				sound.appendChild(fixed);
				
				xml.appendChild(sound);
			}
				
			return xml;
		}
		
		public static function load():void
		{
			fileRef = new FileReference();
			configureListeners(fileRef);
			fileRef.browse([new FileFilter("Canvas Text Files (*.txt)", "*.txt")]);
			status = LOAD;
		}
		
		public static function loadFromSaveFile(fileData:ByteArray):void
		{
			Game.builder.canvas.wipe();
			
			var xml:XML = XML(fileData);
			var pos:Point;
			
			for each (var object:XML in xml.object) {
				if (object.objectType == "sound") {
					pos = new Point(object.position.x, object.position.y);
					Game.builder.canvas.addCanvasSound(pos, object.size.width, object.size.height, object.type);
				}
				else if (object.objectType == "npc") {
					pos = new Point(object.position.x, object.position.y);
					Game.builder.canvas.addCanvasNPC(pos, object.questGiver);
				}
				else {
					var start:Point = new Point(object.start.x, object.start.y);
					var end:Point = new Point(object.end.x, object.end.y);
					Game.builder.canvas.addCanvasObject(start, end, object.name);
				}
			}
		}
		
		private static function configureListeners(dispatcher:IEventDispatcher):void 
		{
			dispatcher.addEventListener(Event.CANCEL, cancelHandler);
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(Event.SELECT, selectHandler);
			dispatcher.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA,uploadCompleteDataHandler);
		}
		
		private static function cancelHandler(event:Event):void {
			trace("cancelHandler: " + event);
		}
		
		private static function completeHandler(event:Event):void {
			trace("completeHandler: " + event);
			var file:FileReference = FileReference(event.target);
			loadFromSaveFile(file.data);
		}
		
		private static function uploadCompleteDataHandler(event:DataEvent):void {
			trace("uploadCompleteData: " + event);
		}
		
		private static function openHandler(event:Event):void {
			trace("openHandler: " + event);
		}
		
		private static function selectHandler(event:Event):void {
			var file:FileReference = FileReference(event.target);
			trace("selectHandler: name = " + file.name);
			if (status == LOAD)
				file.load();
		}
		
	}
}