// author : Joshua Newman
package builder
{
	import ai.Blackboard;
	import ai.NPC;
	
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	
	//import sound.AudioListener;
	/*
	CanvasAvatar
	Our game avatar, the player character, is also our only listener in the world
	related classes AudioListener.as
	*/
	public class CanvasNPC extends Sprite implements IDrawable, IUpdateable, ICanvasObject
	{
		private var _selected:Boolean = false;
		private var randomTime:Number = 0;
		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(val:Boolean):void
		{
			_selected = val;
		}
		
		private var _attributes:Object = new Object();
		public function get attributes():Object
		{
			return _attributes;
		}
		
		public var brain:NPC;
		
		private var move:int = 0;
		
		public var canvas:Canvas;
		
		//public var audioListener:AudioListener;
		
		public function CanvasNPC(canvas:Canvas)
		{
			super();
			
			this.canvas = canvas;
			
			var numNPCs:int = 0;
			
			for each (var npc:CanvasNPC in Game.builder.canvas.npcs) {
				numNPCs += 1;
			}
			
			_attributes.name = "npc" + (numNPCs+1);
			_attributes.questGiver = 0;
			_attributes.mood = 0;
			_attributes.relationship = 0;
			_attributes.type = "friendly";
			_attributes.position = new Vector3D();
			_attributes.facing = new Vector3D(0, 0, 1);
			_attributes.maxSpeed = 2.8;
			
			brain = new NPC(this);
			brain.id = String(0);
			//audioListener = new AudioListener(this);
			
			this.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.ESCAPE)
			{
				// deselect
				canvas.selectedObject = null;
			}
		}
		
		public function rotateTo(x:int, z:int):void
		{
			var to:Vector3D = new Vector3D(x, 0, z);
			_attributes.facing = to.subtract(_attributes.position);
			(_attributes.facing as Vector3D).normalize();
		}
		
		public function moveForward():void
		{
			move = 1;
		}
		
		public function moveBackward():void
		{
			move = -1;
		}
		
		public function stop():void
		{
			move = 0;
		}
		
		public function checkNPCInteractions():void
		{
			// NPC/NPC interaction
			// check if another npc is within this npc's sensing range
			if (canvas.npcs.length > 1) {
				for each (var npc:CanvasNPC in canvas.npcs) {
					var continueCheck:Boolean = false;
					
					for each (var otherNPC:CanvasNPC in canvas.npcs) {
						if (npc != otherNPC) {
							if (npc.getBounds(canvas).intersects(otherNPC.getBounds(canvas))) {
								// make npcs face each other
								npc.rotateTo(otherNPC.attributes.position.x, otherNPC.attributes.position.z);
								otherNPC.rotateTo(npc.attributes.position.x, npc.attributes.position.z);
								
								// update blackboard
								Blackboard.npcInteract = true;
								
								// check if the interacting npcs are already in blackboard
								for each (var arry:Array in Blackboard.interactingNPC) {
									if (arry[0].name == npc.name || arry[1].name == npc.name ||
										arry[0].name == otherNPC.name || arry[1].name == otherNPC.name) {
										continueCheck = true;
										break;
									}
								}
								
								if (continueCheck) {
									continue;
								}
								
								var npcCouple:Array = new Array();
								npcCouple.push(npc);
								npcCouple.push(otherNPC);
								Blackboard.interactingNPC.push(npcCouple);
							}
							else {
								// check if in blackboard, if they are, remove from blackboard
								for each (var couple:Array in Blackboard.interactingNPC) {
									if ((couple[0].name == npc.name || couple[1].name == npc.name) &&
										(couple[0].name == otherNPC.name || couple[1].name == otherNPC.name)) {
										Blackboard.interactingNPC.splice(Blackboard.interactingNPC.indexOf(couple), 1);
									}
								}
							}
						}
					}
				}
			}
		}
		
		public function update(deltaTime:Number):void
		{
			randomTime += deltaTime;
			
			// move the avatar
			x += move * _attributes.facing.x * _attributes.maxSpeed * 1/canvas.ptm_ratio * deltaTime;
			y += move * _attributes.facing.z * _attributes.maxSpeed * 1/canvas.ptm_ratio * deltaTime;
			
			// updates the avatar's position if user drags it around
			_attributes.position.x = this.x;
			_attributes.position.z = this.y;
			
			//audioListener.update(deltaTime);
			
			// make npc face random direction
			if (randomTime > 3) {
				var randomNum:Number = Math.floor(Math.random()*4);

				switch (randomNum) {
					case 0:
						_attributes.facing = new Vector3D(-1,0,0);
						break;
					case 1:
						_attributes.facing = new Vector3D(0,0,-1);
						break;
					case 2:
						_attributes.facing = new Vector3D(1,0,0);
						break;
					case 3:
						_attributes.facing = new Vector3D(0,0,1);
						break;
				}
				// reset randomTime
				randomTime = 0;
			}
			
			// NPC/Player interaction
			// check if avatar is within npc's sensing range
			if (this.getBounds(canvas).intersects(canvas.avatar.getBounds(canvas))) {
				rotateTo(canvas.avatar.x, canvas.avatar.y);
				
				// update blackboard
				Blackboard.nearNPC = true;
				for each (var npcObj:CanvasNPC in Blackboard.aroundNPC) {
						if (npcObj.name == this.name) {
							checkNPCInteractions();
							return;
						}
					}
				Blackboard.aroundNPC.push(this);
			}
			else {
				if (Blackboard.aroundNPC.length == 0) {
					Blackboard.nearNPC = false;
					checkNPCInteractions();
					return;
				}
				for each (var npc:CanvasNPC in Blackboard.aroundNPC) {
					if (npc.name == this.name) {
						Blackboard.aroundNPC.splice(Blackboard.aroundNPC.indexOf(npc), 1);
					}
				}
			}
			
			checkNPCInteractions();
		}
		
		public function draw(deltaTime:Number):void
		{
			graphics.clear();
			var radius:int = 8;
			graphics.beginFill(0x00CC00);
			graphics.drawCircle(0, 0, radius);
			graphics.endFill();
			graphics.lineStyle(3, 0xFFFF00);
			graphics.moveTo(0, 0);
			graphics.lineTo(_attributes.facing.x*radius, _attributes.facing.z*radius);
			graphics.beginFill(0xFFFF00, 0.5);
			graphics.drawCircle(0, 0, radius*4);
			graphics.endFill();
			
			if (_attributes.questGiver == 1) {
				graphics.lineStyle(0, 0x0000FF);
				graphics.beginFill(0x0000FF);
				graphics.moveTo(4, -20);
				graphics.lineTo(-4, -20);
				graphics.lineTo(0, -11);
				graphics.lineTo(4, -20);
			}
		}
	}
}