package builder
{
	import ai.Blackboard;
	import sound.SoundManager;
	
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	
	/*
	CanvasAvatar
	Our game avatar, the player character, is also our only listener in the world
	related classes AudioListener.as
	*/
	public class CanvasSound extends Sprite implements IDrawable, IUpdateable, ICanvasObject
	{
		private var _selected:Boolean = false;
		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(val:Boolean):void
		{
			_selected = val;
		}
		
		private var _attributes:Object = new Object();
		public function get attributes():Object
		{
			return _attributes;
		}
		
		private var move:int = 0;
		
		public var canvas:Canvas;
		
		public function CanvasSound(canvas:Canvas, position:Point)
		{
			super();
			
			this.canvas = canvas;
			this.x = position.x;
			this.y = position.y;
			
			var numSounds:int = 0;
			
			for each (var snd:CanvasSound in Game.builder.canvas.sounds) {
				numSounds += 1;
			}
			
			_attributes.playing = 0;
			_attributes.fixed = 0;
			_attributes.name = "sound" + (numSounds+1);
			_attributes.width = 100;
			_attributes.height = 100;
			_attributes.position = new Vector3D(position.x, 0, position.y);
			
			//audioListener = new AudioListener(this);
			
			this.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.ESCAPE)
			{
				// deselect
				canvas.selectedObject = null;
			}
		}
		
		public function update(deltaTime:Number):void
		{
			
			// updates the avatar's position if user drags it around
			_attributes.position.x = this.x;
			_attributes.position.z = this.y;
			
			// check if player is within sound object then update blackboard
			if (this.getBounds(canvas).intersects(canvas.avatar.getBounds(canvas))) {
				for each (var soundObj:CanvasSound in Blackboard.inSoundObj) {
					if (soundObj.name == this.name) {
						return;
					}
				}
				Blackboard.inSoundObj.push(this);
				switch (_attributes.name) {
					case "town":
						_attributes.playing = SoundManager.sharedManager.change(_attributes.playing, "amb_town");
						break;
					case "forest":
						_attributes.playing = SoundManager.sharedManager.change(_attributes.playing, "amb_forest");
						break;
				}
			}
			else {
				for each (var sndObj:CanvasSound in Blackboard.inSoundObj) {
					if (sndObj.name == this.name) {
						Blackboard.inSoundObj.splice(Blackboard.inSoundObj.indexOf(sndObj), 1);
						SoundManager.sharedManager.stop(_attributes.playing);
					}
				}
			}
		}
		
		public function draw(deltaTime:Number):void
		{
			var alpha:Number;
			var color:int;
			
			switch (_attributes.type) {
				case "ambience":
					alpha = 0.1;
					color = 0x00FF00;
					break;
				case "environmental":
					alpha = 0.3;
					color = 0xFFCC00;
					break;
			}
			graphics.clear();
			graphics.beginFill(color, alpha);
			graphics.drawRect(0, 0, _attributes.width, _attributes.height);
			graphics.endFill();
		}
	}
}