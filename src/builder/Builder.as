// author : Leon Ho
package builder
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	/*
	Builder
	Our world builder, comes with a canvas, and object attribute panel
	related classes Canvas.as, Panel.as
	*/
	public class Builder extends Sprite implements IUpdateable, IDrawable
	{
		private var game:Game;
		
		public var canvas:Canvas;
		public var panel:Panel;
		
		public function Builder(game:Game)
		{
			super();
			
			this.game = game;
			
			game.addChild(this);
			
			canvas = new Canvas();
			addChild(canvas);
			
			panel = new Panel(canvas);
			addChild(panel);
			panel.x = canvas.attributes.size;
		}
	
		public function update(deltaTime:Number):void
		{
			canvas.update(deltaTime);
			panel.update(deltaTime);
		}
		
		public function draw(deltaTime:Number):void
		{
			canvas.draw(deltaTime);
			panel.draw(deltaTime);
		}
	}
}