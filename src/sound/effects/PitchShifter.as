/*
PitchShifter
used for pitch variation on a random container
author: Leon Ho 
reference: http://philippseifried.com/blog/2011/12/27/dynamic-audio-in-as3-part-5-interpolation-and-pitching/
*/
package sound.effects
{
	public class PitchShifter
	{
		protected var currentIndex:Number = 0;
		public var pitch:Number;
		
		public function process(inL:Vector.<Number>, inR:Vector.<Number>, outL:Vector.<Number>, outR:Vector.<Number>):void
		{ 
			processOne(inL, outL);
			processOne(inR, outR);
		}
		
		private function processOne(input:Vector.<Number>, output:Vector.<Number>):void
		{
			currentIndex = 0;
			var numSamples:int = output.length;
			var inputLength:int = input.length - 1;
			
			for (var i:int = 0; i<numSamples; i++)  
			{ 
				currentIndex += pitch; 
				if (currentIndex >= inputLength) currentIndex -= inputLength; 
				
				var index1:int = int(currentIndex); 
				var index2:int = index1+1; 
				var index2Factor:Number = currentIndex-index1; 
				var index1Factor:Number = 1-index2Factor; 
				
				output[i] = input[index1]*index1Factor + input[index2]*index2Factor;
			}
		}
	}
}