/*
Fader
fader for performing fade sound effect, used by sound manager to crossfade between zones
author: Leon Ho
*/
package sound.effects
{
	import sound.SoundUtils;

	public class Fader implements ISoundEffect
	{
		private var from:Number;
		private var to:Number;
		private var duration:Number;
		private var gainMultiplier:Number;
		
		public var elapsed:Number = 0.0;
			
		public function Fader(from:Number, to:Number, duration:Number)
		{
			this.from = from;
			this.to = to;
			this.duration = duration;
		}
		
		public function process(inL:Vector.<Number>, inR:Vector.<Number>, outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			elapsed += n / 44100;
			
			gainMultiplier = SoundUtils.clamp(elapsed/duration * Math.abs(to-from), 0, 1);
			if (from > to) gainMultiplier = 1 - gainMultiplier;
				
			for (var i:int = 0; i < n; i++)
			{
				outL[i] = inL[i]*gainMultiplier;
				outR[i] = inL[i]*gainMultiplier;
			}
		}
	}
}