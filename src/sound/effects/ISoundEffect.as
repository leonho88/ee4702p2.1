package sound.effects
{
	public interface ISoundEffect
	{
		function process(inL:Vector.<Number>, inR:Vector.<Number>, outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void;
	}
}