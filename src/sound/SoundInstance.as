/*
SoundInstance
an instance of a sound based on a sound container
author: Leon Ho
*/
package sound
{
	import sound.container.Container;
	import sound.container.RandomContainer;
	import sound.effects.Fader;

	public class SoundInstance
	{	
		public var id:int;		
		public var fadeDur:Number = 2;
		public var fadeIn:Boolean = true;
		public var fadeOut:Boolean = false;
		public var inFader:Fader = new Fader(0, 1, fadeDur);
		public var outFader:Fader = new Fader(1, 0, fadeDur);
		
		public function SoundInstance(container:Container)
		{
			// get id from SoundManager, then increment for next instance
			id = SoundManager.SoundIDs++;
			
			this.container = container;
			
			if (container is RandomContainer) fadeIn = false;
			
			container.registerConsumer(this);
		}
		
		public function generateAudio(outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{	
			container.generateAudio(this, outL, outR, n);
			
			if (fadeIn)
			{
				inFader.process(outL, outR, outL, outR, n);
				
				if (inFader.elapsed >= fadeDur)
					fadeIn = false;
			}
			
			if (fadeOut)
			{
				outFader.process(outL, outR, outL, outR, n);
				
				if (outFader.elapsed >= fadeDur)
					cleanup();
			}
		}
		
		public function cleanup():void
		{
			container.unregisterConsumer(this);
			
			container.bus.kill(this);
		}
		// pointer to the container
		private var container:Container;
	}
}