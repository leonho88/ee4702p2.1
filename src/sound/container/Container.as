/*
Container
this is the base class of a sound container
it is used to hold pointers to one or many waves in the wave bank
it can be extended to affect how the waves are to be played
author: Leon Ho
*/
package sound.container
{
	import flash.media.Sound;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import sound.Bus;
	import sound.SoundInstance;
	import sound.effects.PitchShifter;

	public class Container
	{
		public var cueName:String;
		public var bus:Bus;
		public var looping:Boolean = false;
		public var gain:Number = 1.0;
		public var gainVariance:Number = 0.0;
		public var pitch:Number = 1.0;
		public var pitchVariance:Number = 0.0;
		
		public function Container(cueName:String)
		{
			this.cueName = cueName;
		}
		
		public function play():void
		{
			// play this sound on the assigned bus
			bus.play(this);
		}
		
		public function registerConsumer(instance:SoundInstance):void
		{
			consumers[instance] = 1;
		}
		
		public function unregisterConsumer(instance:SoundInstance):void
		{
			delete consumers[instance];
		}
		
		public function generateAudio(instance:SoundInstance, outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			throw new Error("Container: must implement generateAudio function");
		}
		
		public function extract(instance:SoundInstance, target:ByteArray, length:Number, startPosition:Number):Number
		{
			throw new Error("Container: must implement extract function");
		}
		
		protected var consumers:Dictionary = new Dictionary();
		protected var pitchShifter:PitchShifter = new PitchShifter();
	}
}