/*
RandomContainer
this is a container that randomly picks a wave to be played
it should also apply a pitch variation (if specified) to prevent sounds from sounding too repetitive
however, i could not get it to work properly, so the code for pitch variation has thus been commented out
author: Leon Ho
*/
package sound.container
{
	import flash.utils.ByteArray;
	
	import sound.SoundInstance;
	import sound.SoundManager;
	import sound.SoundUtils;

	public class RandomContainer extends Container
	{
		public function RandomContainer(cueName:String)
		{
			super(cueName);
		}
		
		public override function generateAudio(instance:SoundInstance, outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			/* NON PITCHSHIFTED CODE */
			var info:RandomContainerInfo = consumers[instance];
			
			var track:Track = info.track;

			var read:int = track.extract(temp, n, info.position);
			info.position += read;
			// if the current track has ended, choose a new wave
			if (read < n)
			{
				track = selectRandomTrack();
				info.track = track;
				if (looping)
					info.position = track.extract(temp, n - read, 0);
				else
					instance.cleanup();
			}
			for (var i:int = 0; i < n; i++)
			{
				outL[i] = temp.readFloat();
				outR[i] = temp.readFloat();
			}
			
			/* PITCHSHIFTED CODE 
			var info:RandomContainerInfo = consumers[instance];
			
			var track:Track = info.track;
			
			// pitch variation
			var randomPitch:Number = SoundUtils.randRange(pitch, pitch+pitchVariance);
			var numSamples:int = Math.floor(n*pitch);
			
			var read:int = track.extract(temp, numSamples, info.position);
			info.position += read;
			// if the current track has ended, choose a new wave
			if (read < numSamples)
			{
				track = selectRandomTrack();
				info.track = track;
				if (looping)
					info.position = track.extract(temp, numSamples - read, 0);
				else
					instance.cleanup();
			}
			tempL = new Vector.<Number>(numSamples);
			tempR = new Vector.<Number>(numSamples);
			temp.position = 0;
			for (var i:int = 0; i < numSamples; i++)
			{
				tempL[i] = temp.readFloat();
				tempR[i] = temp.readFloat();
			}
			temp.position = 0;
			
			pitchShifter.pitch = randomPitch;
			pitchShifter.process(tempL, tempR, outL, outR);
			*/
		}
		
		public override function registerConsumer(instance:SoundInstance):void
		{
			consumers[instance] = new RandomContainerInfo(selectRandomTrack());
		}
		
		public function addTrack(track:Track):void
		{
			tracks.push(track);
		}
		
		private function selectRandomTrack():Track
		{
			return tracks[int(Math.random()*tracks.length)];
		}
		
		private var temp:ByteArray = new ByteArray();
		
		private var tempL:Vector.<Number>;
		private var tempR:Vector.<Number>;
		
		private var tracks:Vector.<Track> = new Vector.<Track>();
	}
}