/*
Track
a track is a wrapper around a wave for added functionality
author: Leon Ho
*/
package sound.container
{
	import flash.media.Sound;
	import flash.utils.ByteArray;
	
	import sound.SoundUtils;

	public class Track
	{
		public var startPosition:Number;
		public var endPosition:Number;
		
		public function Track(wave:Sound, startingPosition:Number = 0.0, endingPosition:Number = 1.0)
		{
			this.wave = wave;
			this.startPosition = startingPosition;
			this.endPosition = endingPosition;
		}
		
		public function extract(target:ByteArray, length:Number, startPosition:Number):Number
		{
			return wave.extract(target, length, startPosition);
		}
		
		private var wave:Sound;
	}
}