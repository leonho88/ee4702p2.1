/*
BlendContainer
a container that currently supports 3 horizontal tracks, 1 vertical track and 1 RTPC
this is to support the ambient, tension, action tracks, and transition mask
the audio generated depends on the current position (which may be affected by RTPCs)
author: Leon Ho
*/
package sound.container
{
	import flash.utils.ByteArray;
	
	import sound.RTPC;
	import sound.SoundInstance;
	import sound.SoundUtils;

	public class BlendContainer extends Container
	{
		// RTPC uses this to change playing position of this blendcontainer
		public var position:Number = 0.0;
		
		public function BlendContainer(cueName:String)
		{
			super(cueName);
			looping = true;
		}
		
		public override function generateAudio(instance:SoundInstance, outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			var info:BlendContainerInfo = consumers[instance];
			
			var mixFactor:Number = 1.0;
			for (var i:int = 0; i < horizontal.length; i++)
			{
				var track:Track = horizontal[i];
				// if playing position lies within the current track
				if (track.startPosition <= position && position <= track.endPosition)
				{	
					accL = new Vector.<Number>(n);
					accR = new Vector.<Number>(n);
					// read data from the first track
					var read:int = track.extract(temp, n, info.horizontalPositions[i]);
					info.horizontalPositions[i] += read;
					if (read < n)
					{
						info.horizontalPositions[i] = track.extract(temp, n - read, 0);
					}
					temp.position = 0;
					for (var j:int = 0; j < n; j++)
					{
						// accumulate first track's data
						accL[j] = temp.readFloat();
						accR[j] = temp.readFloat();
					}
					temp.position = 0;
					
					// check if there is a next track
					if (i+1 < horizontal.length)
					{
						var nextTrack:Track = horizontal[i+1];
						// if the playing position lies within the next track as well
						if (nextTrack.startPosition <= position && position <= nextTrack.endPosition)
						{
							++i;
							// only allow mixing of 2 horizontal tracks
							mixFactor = 0.5;
							
							// read data from next track
							read = nextTrack.extract(temp, n, info.horizontalPositions[i]);
							info.horizontalPositions[i] += read;
							if (read < n)
							{
								info.horizontalPositions[i] = nextTrack.extract(temp, n - read, 0);
							}
							temp.position = 0;
							// accumulate second track's data
							for (j = 0; j < n; j++)
							{
								// perform simple linear crossfade
								var fadeIn:Number = SoundUtils.fadeIn(position, nextTrack.startPosition, track.endPosition);
								var fadeOut:Number = 1.0 - fadeIn;
								accL[j] = (fadeOut*mixFactor*accL[j] + fadeIn*mixFactor*temp.readFloat())/mixFactor;
								accR[j] = (fadeOut*mixFactor*accR[j] + fadeIn*mixFactor*temp.readFloat())/mixFactor;
							}
							temp.position = 0;
						}
					}
				}
			}
			/*
			for (i = 0; i < vertical.length; i++)
			{
				track = vertical[i];
				if (track.startPosition <= position && position <= track.endPosition)
				{
					read = track.extract(temp, length, info.verticalPositions[i]);
					info.verticalPositions[i] += read;
				}
			}
			*/
			for (j = 0; j < n; j++)
			{
				outL[j] = accL[j];
				outR[j] = accR[j];
			}
		}
		
		public override function registerConsumer(instance:SoundInstance):void
		{
			consumers[instance] = new BlendContainerInfo(horizontal.length, vertical.length);
		}
		
		public function addHorizontalTrack(track:Track, startPosition:Number = 0.0, endPosition:Number = 1.0):void
		{
			track.startPosition = startPosition;
			track.endPosition = endPosition;
			horizontal.push(track);
		}
		
		public function addVerticalTrack(track:Track, startPosition:Number = 0.0, endPosition:Number = 1.0):void
		{
			track.startPosition = startPosition;
			track.endPosition = endPosition;
			vertical.push(track);
		}
		
		private var temp:ByteArray = new ByteArray();

		private var accL:Vector.<Number>;
		private var accR:Vector.<Number>;
		
		private var horizontal:Vector.<Track> = new Vector.<Track>();
		private var vertical:Vector.<Track> = new Vector.<Track>();
	}
}