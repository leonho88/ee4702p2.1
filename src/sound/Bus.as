/*
Bus
a common way in the industry used to manage related sounds in related groups
author: Leon Ho
*/
package sound
{
	import sound.container.Container;

	public class Bus
	{
		public var busName:String;
		public var busVolume:Number = 1.0;
		public var children:Vector.<Bus> = new Vector.<Bus>();
		public var soundInstances:Vector.<SoundInstance> = new Vector.<SoundInstance>();
		
		public function Bus(busName:String)
		{
			this.busName = busName;
		}
		
		public function addChild(bus:Bus):void
		{
			children.push(bus);	
		}
		
		public function getChildByName(busName:String):Bus
		{
			var retVal:Bus = null;
			for each (var child:Bus in children)
			{
				if (child.busName == busName) retVal = child;
				else retVal = child.getChildByName(busName);
				
				if (retVal) break;
			}
			return retVal;
		}
		
		public function play(container:Container):void
		{
			// create a new instance of the sound for the given container, and add it to the playing sounds
			var instance:SoundInstance = new SoundInstance(container);
			soundInstances.push(instance);
		}
		
		public function stop(id:int):void
		{
			// stop the sound if in this bus
			for each (var instance:SoundInstance in soundInstances)
			{
				if (instance.id == id)
				{
					instance.fadeOut = true;
					return;
				}
			}
			// if not in current bus, find in child buses
			for each (var child:Bus in children)
			{
				child.stop(id);
			}
		}
		
		public function kill(instance:SoundInstance):void
		{
			soundInstances.splice(soundInstances.indexOf(instance), 1);
		}
		
		public function generateAudio(outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			var sources:int = children.length + soundInstances.length;
			
			for (i = 0; i < n; i++)
			{
				acc_bufL[i] = 0;
				acc_bufR[i] = 0;
			}
			
			// flush the output buffers
			if (sources == 0) 
			{
				for (i = 0; i < n; i++)
				{
					outL[i] = 0;
					outR[i] = 0;
				}
				return;
			}
				
			// equal power mixing over all buses and sound instances
			var mixFactor:Number = 1.0 / sources;
		
			var i:int = 0;
			for each (var child:Bus in children)
			{
				child.generateAudio(temp_bufL, temp_bufR, n);
				
				for (i = 0; i < n; i++)
				{
					acc_bufL[i] += mixFactor*temp_bufL[i];
					acc_bufR[i] += mixFactor*temp_bufR[i];
				}
			}
			for each (var soundInstance:SoundInstance in soundInstances)
			{
				soundInstance.generateAudio(temp_bufL, temp_bufR, n);
				
				for (i = 0; i < n; i++)
				{
					acc_bufL[i] += mixFactor*temp_bufL[i];
					acc_bufR[i] += mixFactor*temp_bufR[i];
				}
			}
			
			for (i = 0; i < n; i++)
			{
				outL[i] = busVolume * (acc_bufL[i] / mixFactor);
				outR[i] = busVolume * (acc_bufR[i] / mixFactor);
			}
		}
		
		private var acc_bufL:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
		private var acc_bufR:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
		
		private var temp_bufL:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
		private var temp_bufR:Vector.<Number> = new Vector.<Number>(SoundManager.CHUNK_LEN);
	}
}