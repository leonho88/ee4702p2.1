/*
RTPC
real time parameter control for a blend container
use this to map a RTPC to a blend container
author: Leon Ho
*/
package sound
{
	import flash.utils.Dictionary;
	
	import sound.container.BlendContainer;

	public class RTPC
	{
		public static var map:Dictionary = new Dictionary();
		
		public function RTPC(name:String, container:BlendContainer)
		{
			this.name = name;
			
			this.container = container
		}
		
		public function set value(val:Number):void
		{
			container.position = val;
		}
		
		public function get value():Number
		{
			return map[name].container.position;
		}
		
		public static function addRTPC(name:String, container:BlendContainer):void
		{
			map[name] = new RTPC(name, container);
		}
		
		public static function getRTPC(name:String):RTPC
		{
			return map[name];
		}
		
		private var name:String;
		private var container:BlendContainer;
	}
}