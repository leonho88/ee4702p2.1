/*
SoundBank
this class is used to create sound containers that contain one or many waves from the wavebank
the containers are given a cue name, which is used by the sound manager to play it
different kinds of containers will cause the waves to be play differently
nomenclature and architecture inspired by Wwise
author: Leon Ho
*/
package sound
{
	import flash.utils.Dictionary;
	
	import sound.container.Container;

	public class SoundBank
	{
		public function SoundBank(waveBank:WaveBank)
		{
			this.waveBank = waveBank;	
		}

		public function addSound(container:Container):void
		{
			sounds[container.cueName] = container;
		}
		
		public function play(cueName:String):void
		{
			if (sounds[cueName]) (sounds[cueName] as Container).play();
			else throw new Error("Sound Bank: " + cueName + " not in sound bank.");
		}

		private var waveBank:WaveBank;
		private var sounds:Dictionary = new Dictionary();
	}
}