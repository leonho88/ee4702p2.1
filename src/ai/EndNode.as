package ai
{
	public class EndNode extends TreeNode
	{
		public function EndNode()
		{
			super();
		}
		
		override public function run(owner:*):TreeNode
		{
			// pass null msg
			owner.say(null);
			// restart the dialog tree
			return owner.root;
		}
	}
}