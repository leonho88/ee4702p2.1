package ai
{
	import flash.utils.Dictionary;

	public class CommandNode extends TreeNode
	{
		public var commands:Vector.<String> = new Vector.<String>();
		
		public function CommandNode()
		{
		}
		
		public function addValue(cmd:String):void
		{
			commands.push(cmd);
		}
		
		public override function run(owner:*):TreeNode
		{
			for each (var command:String in commands)
			{
				var line:Array = command.split(" ");
				
				var target:* = null;
				if (line[0] == "self")
					target = owner;
				else if (line[0] == "bb")
					target = owner.bb;
				
				var func:Function = target[line[1]] as Function;
				var args:Array = line.slice(2);
				processCommand(func, args);
			}
			return next.run(owner);
		}
		
		private function processCommand(command:Function, args:Array):void
		{
			try 
			{
				switch (args.length)
				{
					case 0:
						command();
						break;
					case 1:
						command(args[0]);
						break;
					case 2:
						command(args[0], args[1]);
						break;
					case 3:
						command(args[0], args[1], args[2]);
						break;
					case 4:
						command(args[0], args[1], args[2], args[3]);
						break;
					case 5:
						command(args[0], args[1], args[2], args[3], args[4]);
						break;
				}
			}
			catch (e:Error)
			{
				trace(e.message);
			}
		}
	}
}