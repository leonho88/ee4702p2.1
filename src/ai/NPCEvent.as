package ai
{
	import flash.events.Event;
	
	public class NPCEvent extends Event
	{
		public static const INTERRUPT:String = "interrupt";
		public static const TIMEOUT:String = "timeout";
		public static const INPUT:String = "input";
		
		public var input:String;
		
		public function NPCEvent(type:String, input:String = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.input = input;
			super(type, bubbles, cancelable);
		}
	}
}