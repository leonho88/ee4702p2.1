package ai
{
	import ai.Blackboard;
	
	import builder.Canvas;
	import builder.CanvasAvatar;
	import builder.CanvasNPC;
	
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import flashx.textLayout.tlf_internal;
	
	import sound.RTPC;
	import sound.SoundManager;
	
	public class Manager extends EventDispatcher
	{
		private var canvas:Canvas;
		private var oldNumNPC:int = 0;
		private var npcDialogPref:String;
		private var _currMsg:String;
		private var _currChoices:Array;
		private var oldTension:Number = Blackboard.tension;
		private var interpolate:Number = 0;
		
		private var elapsedTime:Number = 0;
		private const npcTolerance:Number = 15.0;
		
		private var lastNpcState:CanvasNPC = null;

		public function Manager(canvas:Canvas)
		{
			this.canvas = canvas;
		}
		
		public function set currMsg(msg:String):void
		{
			_currMsg = msg;
		}
		
		public function set currChoices(choices:Array):void
		{
			_currChoices = choices;
		}
		
		public function updateTension(deltaTime:Number):void
		{
			var playerCurPos:Vector3D = Blackboard.playerCurPos;
			var newTension:Number = 0;
			var numNPCInRange:int = 0;
			
			for each (var npc:CanvasNPC in canvas.npcs) {
				if (Vector3D.distance(npc.attributes.position, playerCurPos) <= Blackboard.tensionRange) {
					if (npc.attributes.type == "hostile") {
						numNPCInRange++;
					}
				}
			}
			
			// increase tension when at least 1 npc within range
			if (numNPCInRange > 0) {
				newTension = 1;
			}
			else {
				newTension = 0;
			}
			
			if (oldNumNPC != numNPCInRange) {
				oldTension = Blackboard.tension;
				oldNumNPC = numNPCInRange;
				
				if (newTension > oldTension) {
					interpolate = 0;
				}
				else {
					interpolate = 1;
				}
			}
						
			// interpolate tension based on num of npcs in range
			var newPt:Point = new Point(newTension, 0);
			var oldPt:Point = new Point(oldTension, 0);
			
			var interpolatedPt:Point = new Point();
			
			if (newTension > oldTension) {
				interpolatedPt = Point.interpolate(newPt, oldPt, interpolate);
			}
			else {
				interpolatedPt = Point.interpolate(oldPt, newPt, interpolate);				
			}
			
			Blackboard.tension = interpolatedPt.x;
			
			if (newTension != oldTension) {
				if (newTension > oldTension) {
					if (interpolate + deltaTime*0.065*numNPCInRange < 1) {
						interpolate += deltaTime*0.065*numNPCInRange;
					}
					else {
						oldTension = newTension;
					}
				}
				else {
					if (interpolate - deltaTime > 0) {
						interpolate -= deltaTime*0.065;
					}
					else {
						oldTension = newTension;
					}
				}
			}
			
			if (SoundManager.READY)
			{
				RTPC.getRTPC("tension").value = Blackboard.tension;
			}
		}
		
		public function updateDialog(deltaTime:Number):String
		{
			var dialogStr:String = "";
			
			lastNpcState = Blackboard.currNPCInDialog;
			
			if (!Blackboard.nearNPC) {
				Blackboard.currNPCInDialog = null;
			}
			
			
			if (lastNpcState && !Blackboard.currNPCInDialog)
			{
				_currMsg = null;
				_currChoices = null;
				lastNpcState.brain.dispatchEvent(new NPCEvent(NPCEvent.INTERRUPT));
				return dialogStr;
			}
			if (Blackboard.currNPCInDialog != null && !_currMsg)
				Blackboard.currNPCInDialog = null;
			
			// check if any npc currently in dialog with player and if not check how many npcs are around player to initiate dialog
			if (Blackboard.currNPCInDialog != null && _currMsg) {				
				dialogStr = _currMsg;
				if (_currChoices)
				{
					var i:int;
					for (i=0; i < _currChoices.length; i++) {
						var choice:String = _currChoices[i];
						dialogStr = dialogStr.concat("\n[" + (i+1) + "] " + choice);
					}
				}
				
				elapsedTime += deltaTime;
				if (elapsedTime > npcTolerance)
				{
					elapsedTime = 0;
					Blackboard.currNPCInDialog.brain.dispatchEvent(new NPCEvent(NPCEvent.TIMEOUT));
				}
			}
			else {
				if (Blackboard.aroundNPC.length > 1) {
					dialogStr = "Choose which player to talk to:\n\n";
					var i:int;
					for (i=0; i < Blackboard.aroundNPC.length; i++) {
						var npc:CanvasNPC = Blackboard.aroundNPC[i];
						dialogStr = dialogStr.concat("[" + (i+1) + "] " + npc.attributes.name + "\n");
					}
					return dialogStr;
				}
				else {
					if (Blackboard.aroundNPC.length > 0) {
						dialogStr = "Press 1 to talk to " + Blackboard.aroundNPC[0].attributes.name;
					}
					else {
						Blackboard.currNPCInDialog = null;
						dialogStr = "";
					}
				}
			}
			
			return dialogStr;
		}
		
		public function makeChoice(choice:int):void
		{
			// deciding to initiate dialog
			if (Blackboard.currNPCInDialog == null) {
				npcDialogPref = Blackboard.aroundNPC[choice-1].attributes.name;
				startDialog();
			}
			// making choices during dialog
			else {
				if (choice == 0)
					Blackboard.currNPCInDialog.brain.think();
				else
				{
					var input:String = _currChoices[choice-1];
					_currChoices = null;
					Blackboard.currNPCInDialog.brain.dispatchEvent(new NPCEvent(NPCEvent.INPUT, input));
				}
				elapsedTime = 0;
			}
		}
		
		public function startDialog():void
		{
			if (Blackboard.aroundNPC.length > 1) {
				for each (var npc:CanvasNPC in Blackboard.aroundNPC)
				{
					if (npc.attributes.name == npcDialogPref)
						Blackboard.currNPCInDialog = npc;
				}
			}
			else {
				Blackboard.currNPCInDialog = Blackboard.aroundNPC[0];
			}
			Blackboard.currNPCInDialog.brain.think();
		}
	}
}