package ai
{
	import builder.CanvasNPC;
	import builder.CanvasSound;
	
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	
	public class Blackboard
	{
		public static const tensionRange:Number = 200;
			
		public static var tension:Number = 0;
		public static var nearNPC:Boolean;
		public static var npcInteract:Boolean;
		public static var currNPCInDialog:CanvasNPC = null;
		public static var playerCurPos:Vector3D = new Vector3D();
		public static var aroundNPC:Vector.<CanvasNPC> = new Vector.<CanvasNPC>;
		public static var inSoundObj:Vector.<CanvasSound> = new Vector.<CanvasSound>;
		public static var interactingNPC:Vector.<Array> = new Vector.<Array>;
		
		private static var _instance:Blackboard = null;
		public var properties:Dictionary = new Dictionary();
		
		public function setProperty(prop:String, val:*):void
		{
			properties[prop] = val;
		}
		
		public function getProperty(prop:String):*
		{
			return properties[prop];
		}
		
		public function incProperty(prop:String, val:Number):void
		{
			properties[prop] += val;
		}
		
		public function decProperty(prop:String, val:Number):void
		{
			properties[prop] -= val;
		}
		
		public static function sharedBB():Blackboard
		{
			if (!_instance) _instance = new Blackboard();
			return _instance;
		}
		
		public function Blackboard()
		{
			// default properties
			setProperty("questCompleted", "false");
		}
	}
}