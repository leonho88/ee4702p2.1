package ai
{
	import flash.utils.Dictionary;

	public class SwitchNode extends TreeNode
	{
		public var condition:String; // the condition we want to evaluate
		public var map:Dictionary = new Dictionary(); // maps expected condition value to corresponding tree node, USE STRING VALUES HERE
		
		public function SwitchNode(condition:String)
		{
			this.condition = condition;
		}
		
		public function addValue(value:String, node:TreeNode):void
		{
			map[value] = node;
		}
		
		public override function run(owner:*):TreeNode
		{
			var line:Array = condition.split(" ");
			
			var target:* = null;
			if (line[0] == "self")
				target = owner;
			else if (line[0] == "bb")
				target = owner.bb;
			
			return (map[target.getProperty(line[1])] as TreeNode).run(owner);
		}
	}
}