package ai
{
	public class DialogNode extends TreeNode
	{
		public var msg:String;
		
		public function DialogNode(msg:String)
		{
			this.msg = msg;
		}
		
		public override function run(owner:*):TreeNode
		{
			owner.say(msg);
			
			return next;
		}
	}
}