package ai
{
	import flash.utils.Dictionary;

	public class PseudoNPC
	{
		public var root:TreeNode;
		public var nextNode:TreeNode;
		public var dialogMap:Dictionary = new Dictionary();

		public var id:int = 1;
		
		public var questGiven:String = "false";
		public var questDone:String = "false";
		
		public var polite:String = "false";
		
		public var stranger:String = "true";
		
		public var relationship:int = 100;
		
		public function PseudoNPC()
		{
			createTree();
		}
		
		public function createTree():void
		{
			// choose the npc's tree
			var dialogTree:* = Dialogue.dialog.NPC.(@id == id);
			for (var i:int = 0; i < dialogTree.node.length(); i++)
			{
				trace ("ID: " + dialogTree.node[i].@id + "\n" + dialogTree.node[i]);
				dialogMap[String(dialogTree.node[i].@id)] = parseTree(dialogTree.node[i]);
			}
			
			for (var k:Object in dialogMap) {
				var node:TreeNode = dialogMap[k];
				var value:String = String(k);
				trace(value, node);
			}
			
			this.root = dialogMap[String(dialogTree.node[0].@id)];
			this.nextNode = this.root;
		}
		
		private function parseTree(dialogTree:*):TreeNode
		{
			var node:TreeNode = null;
			
			var i:int = 0;
			
			if (dialogTree.@type == "null")
			{
				node = null;
			}
			else if (dialogTree.@type == "condition")
			{
				node =  new SwitchNode(dialogTree.@condition);
				for (i = 0; i < dialogTree.value.length(); i++)
				{
					(node as SwitchNode).addValue(dialogTree.value[i].@value, parseTree(dialogTree.value[i].node));
				}
			}
			else if (dialogTree.@type == "dialog")
			{
				node = new DialogNode(dialogTree.msg);
				node.next = parseTree(dialogTree.next.node);
			}
			else if (dialogTree.@type == "choice")
			{
				node = new ChoiceNode(dialogTree.question);
				for (i = 0; i < dialogTree.value.length(); i++)
				{
					(node as ChoiceNode).addValue(dialogTree.value[i].@value, parseTree(dialogTree.value[i].node));
				}
			}
			else if (dialogTree.@type == "jump")
			{
				node = new JumpNode(dialogTree, dialogMap);
			}
			else if (dialogTree.@type == "command")
			{
				node = new CommandNode();
				for (i = 0; i < dialogTree.cmd.length(); i++)
				{
					(node as CommandNode).addValue(dialogTree.cmd[i]);
				}
				node.next = parseTree(dialogTree.next.node);
			}
			return node;
		}
		
		public function say(msg:String):void
		{
			trace("NPC " + id + " says: " +  msg);
		}
		
		public function prompt(question:String, choices:Array):String
		{
			trace(question + choices);
			//	trace(question + " buy sword" + ", " + "buy potion" + ", " + "nothing");
			return choices[int(Math.random()*choices.length)];
		}
		
		// set property
		public function setProperty(prop:String, val:*):void
		{
			this[prop] = val;
		}
		
		// increment property
		public function incProperty(prop:String, val:Number):void
		{
			this[prop] += val;
		}
		
		// decrement property
		public function decProperty(prop:String, val:Number):void
		{
			this[prop] -= val;
		}
		
		public function giveSword(amt:int):void
		{
			trace("Give player: " + amt + " swords.");
		}
		
		public function givePotion(amt:int):void
		{
			trace("Give player: " + amt + " potions.");
		}
		
		public function takeMoney(amt:int):void
		{
			trace("Take " + amt + " gold from player.");
		}
		
		public function think():void
		{
			// if no more next node, restart from root
			// else run the next node
			if (!nextNode)
				nextNode = root;
			else 
				nextNode = nextNode.run(this);
		}
	}
}